# SRFI 113: Sets and bags

**Author**: John Cowan

**SRFI Page**: <https://srfi.schemers.org/srfi-113/srfi-113.html>

If you'd like to participate in the discussion of this SRFI, or report issues with it, please [join
the SRFI-113 mailing list](https://srfi.schemers.org/srfi-113/) and send your message there.

## Implementation

The repository hosts a complete implementation of the SRFI, running on CHICKEN Scheme.

## License

Provided under a single clause BSD license, Copyright (C) John Cowan 2016. See LICENSE for full
details.
