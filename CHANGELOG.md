# SRFI-113 Changelog

## 1.2.0
### Changed
- The egg is now officially an r7rs egg!
- Source layout has changed. `sets/` has been moved to `src/` and test files have all been moved to
  `tests/`. Likewise, `srfi-113.scm` is gone, replaced by `src/sets.sld`.
- The repository is now on GitLab.

### Removed
- Many redundant files that aren't necessary to clone on every release have been removed. This
  includes HTML files that mimic the SRFI-113 reference, as well as just consolidating source files
  into their appropriate locations.

### Fixed
- The project now has its own CI setup on GitLab, and utilizes `chicken-lint` from the beaker egg.
- Lints from `chicken-lint` have been fixed. Mostly this was just the result of replacing
  one-armed-if usage with `when` and `unless` where appropriate.

## 1.1
### Fixed
- Fix broken S-expr from dyadic-sob<? (bad merge?)

## 1.0
### Fixed
- Fix pure-subset comparison, dyadic-sob<? and dyadic-sob>? (thanks shirok) [BROKEN]

The 1.0 release is broken and should be pulled, so please upgrade to 1.1 or newer.

## 0.15
### Added
- Adds read & printer syntax, as well as set-sob-read-syntax-comparator!

## 0.14
### Fixed
- Fixes tests

## 0.13
### Added
- Adds srfi-113.scm to .meta file (for CHICKEN 4)

## 0.12
### Added
- Adds .egg file to .meta list (for CHICKEN 4)

## 0.11
### Added
- CHICKEN 5 support, with fixed tests
