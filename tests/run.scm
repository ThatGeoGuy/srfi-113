(cond-expand
  (chicken-5
   (import test
           (only (srfi 13) string-downcase)
           (srfi 113)
           (srfi 128))))

(include "comparators-shim.scm")
(include "sets-test.scm")
